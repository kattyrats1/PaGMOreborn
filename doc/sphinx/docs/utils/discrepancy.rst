.. cpp_constrained_optimization

Low-discrepancy sequences
=========================

A number of utilities to generate and use low- discrepancy sequences

--------------------------------------------------------------------------

.. doxygenfunction:: pagmo::sample_from_simplex
   :project: PaGMOreborn

--------------------------------------------------------------------------

.. doxygenclass:: pagmo::van_der_corput
   :project: PaGMOreborn
   :members:

--------------------------------------------------------------------------

.. doxygenclass:: pagmo::halton
   :project: PaGMOreborn
   :members:






