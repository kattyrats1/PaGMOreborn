#ifndef PAGMO_CONSTANTS_HPP
#define PAGMO_CONSTANTS_HPP

namespace pagmo { namespace detail {

constexpr double pi()
{
    return 3.1415926535897932384626433832795028841971693993;
}

}}

#endif